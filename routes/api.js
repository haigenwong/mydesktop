var express = require('express');
var router = express.Router();

/* GET api. */
router.get('/', function(req, res) {
    console.info("=======");
    res.render('index', { title: '后台管理' });
});

module.exports = router;
